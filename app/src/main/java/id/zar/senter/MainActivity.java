package id.zar.senter;


import android.app.Activity;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.health.PackageHealthStats;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Switch;

public class MainActivity extends AppCompatActivity {
    private Switch saklar;
    private Camera camera;
    private Activity activity;
    private boolean torchIsOn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;

        setContentView(R.layout.activity_main);
        saklar = (Switch) findViewById(R.id.saklar_senter);

        saklar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(saklar.isChecked()){
                    nyalakan();
                }else{
                    matikan();
                }
            }
        });

    }

    private void matikan() {
        if(camera != null){
            camera.stopPreview();
            camera.release();
            torchIsOn = false;
        }
    }
    private void nyalakan(){
        if(activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
            camera = Camera.open();
            Camera.Parameters param = camera.getParameters();
            param.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            camera.setParameters(param);
            camera.startPreview();
            torchIsOn = true;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(torchIsOn) {
            matikan();
        }
    }
}
